package server.exchange;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import server.common.model.ExchangeRate;

/**
 * A persistent repository that contains information about {@link
 * ExchangeRate} objects.  The {@code @Repository} annotation
 * indicates that this class provides the mechanism for storage,
 * retrieval, search, update and delete operation on {@link
 * ExchangeRate} objects.
 */
@Repository
public interface ExchangeRepository extends JpaRepository<ExchangeRate, Long> {
    /**
     * Returns the {@link ExchangeRate} that matches the {@code
     * fromCurrency} and {@code toCurrency} query parameters.
     *
     * @param fromCurrency The 3 letter currency code to exchange from.
     * @param toCurrency   The 3 letter currency code to exchange to.
     * @return An {@link ExchangeRate} object containing the passed
     * parameters and a double exchange rate value
     */
    ExchangeRate findByFromCurrencyAndToCurrency(@Param("fromCurrency") String fromCurrency,
                                                 @Param("toCurrency") String toCurrency);
}
