package server.exchange;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import server.common.model.ExchangeRate;

import static server.common.Constants.EndPoint.RATE;

/**
 * This Spring controller demonstrates how Spring MVC can be used to
 * handle HTTP GET requests via Java functional programming.  These
 * requests are mapped to a method that forwards the requests to the
 * {@link ExchangeService}, which finds and returns the current
 * exchange rate for various currencies.
 *
 * In Spring's approach to building RESTful web services, HTTP
 * requests are handled by a controller (identified by the
 * {@code @RestController} annotation) that defines the endpoints (aka
 * routes) for each supported operation, i.e., {@code @GetMapping},
 * {@code @PostMapping}, {@code @PutMapping}, and
 * {@code @DeleteMapping}, which correspond to the HTTP GET, POST,
 * PUT, and DELETE calls, respectively.
 *
 * Spring uses the {@code @GetMapping} annotation to map HTTP GET
 * requests onto methods in the {@link ExchangeController}.  GET
 * requests invoked from any HTTP web client (e.g., a web browser or
 * Android app) or command-line utility (e.g., Curl or Postman).
 *
 * The {@code @CrossOrigin} annotation marks the annotated method or
 * type as permitting cross origin requests, which is required for
 * Eureka redirection.
 */
@RestController
@CrossOrigin("*") // Required for Eureka redirection
public class ExchangeController {
    /**
     * This auto-wired field connects the {@link ExchangeController}
     * to the {@link ExchangeService}.
     */
    @Autowired
    ExchangeService service;

    /**
     * Request used by Eureka Control panel.
     *
     * @return Print something useful (e.g. class name).
     */
    @GetMapping("/actuator/info")
    public String info() {
        return getClass().getName();
    }

    /**
     * Forwards the request to the exchange microservice to find and
     * return the exchange rate that matches the {@code fromCurrency}
     * and {@code toCurrency} query parameters.
     *
     * @param fromCurrency The 3 letter currency code to exchange from.
     * @param toCurrency   The 3 letter currency code to exchange to.
     * @return A {@link ResponseEntity} that encapsulates an {@link
     *         ExchangeRate} object containing the passed parameters
     *         and a double exchange rate value
     */
    @GetMapping(RATE)
    public ResponseEntity<ExchangeRate> getRate(@RequestParam String fromCurrency,
                                                @RequestParam String toCurrency) {
        // Get the ExchangeRate for fromCurrency and toCurrency.
        ExchangeRate exchangeRate = service
            // Forward to the ExchangeService.
            .getRate(fromCurrency, toCurrency);

        if (exchangeRate != null) {
            // Return the exchangeRate successfully.
            return ResponseEntity.ok(exchangeRate);
        } else
            // Indicate a failure.
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
