package server.exchange;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import server.common.Constants;
import server.common.model.ExchangeRate;

/**
 * This class provides the entry point for the currency exchange
 * microservice, which finds and returns the current exchange rate for
 * currencies.
 *
 * The {@code @SpringBootApplication} annotation enables apps to use
 * auto-configuration, component scan, and to define extra
 * configurations on their "application" class.  
 *
 * The {@code @EnableDiscoveryClient} annotation enables service
 * registration and discovery, i.e., this process registers itself
 * with the discovery-server service using its application name. 
 *
 * The {@code @EntityScan} annotation is used when entity classes are
 * not placed in the main application package or its sub-packages. 
 *
 * The {@code @ComponentScan} annotation tells Spring the packages to
 * scan for annotated components (i.e., tagged with
 * {@code @Component}).
 * 
 * The {@code @EnableJpaRepositories} annotation enables the use of
 * JPA repositories by scanning the package of the annotated
 * configuration class for Spring Data repositories.
 *
 * The {@code @PropertySources} annotation is used to provide
 * properties files to Spring Environment.
 */
@SpringBootApplication
@EnableDiscoveryClient
@EntityScan(basePackageClasses = ExchangeRate.class)
@ComponentScan(basePackageClasses = ExchangeApplication.class)
@EnableJpaRepositories(basePackageClasses = ExchangeRepository.class)
@PropertySources({
        @PropertySource(Constants.Resources.EUREKA_CLIENT_PROPERTIES),
        @PropertySource(Constants.Resources.DATABASE_PROPERTIES),
        @PropertySource("classpath:/exchange/exchange.properties")
})
public class ExchangeApplication {
    /**
     * A static main() entry point is needed to run the Exchange
     * microservice.
     */
    public static void main(String[] args) {
        // Launch this microservice through Spring Boot.
        SpringApplication.run(ExchangeApplication.class, args);
    }
}
