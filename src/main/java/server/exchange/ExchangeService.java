package server.exchange;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import server.common.model.ExchangeRate;

/**
 * This class defines an implementation method that is called by the
 * {@link ExchangeController}.  This method finds and returns the
 * current exchange rate for various currencies.
 * 
 * It is annotated as a Spring {@code @Service}, which enables the
 * autodetection and wiring of dependent implementation classes via
 * classpath scanning.
 */
@Service
public class ExchangeService {
    /**
     * An auto-wired field that connects the {@link ExchangeService}
     * to the {@link ExchangeRepository} that stores persistent
     * ExchangeRate objects.
     */
    @Autowired
    ExchangeRepository repository;

    /**
     * Find the exchange rate that matches the {@code fromCurrency}
     * and {@code toCurrency} query parameters.
     *
     * @param fromCurrency The 3 letter currency code to exchange from.
     * @param toCurrency   The 3 letter currency code to exchange to.
     * @return An {@link ExchangeRate} object containing the passed
     *         parameters and a double exchange rate value
     */
    public ExchangeRate getRate(String fromCurrency,
                                String toCurrency) {
        return repository
            // Forward to the persistent repository.
            .findByFromCurrencyAndToCurrency(fromCurrency,
                                             toCurrency);
    }
}
