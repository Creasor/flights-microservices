package server.airport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import server.common.model.Airport;

/**
 * This class defines an implementation method that is called by the
 * {@link AirportController}.  This method synchronously returns a
 * {@link List} of all known {@code Airport} objects.
 * 
 * It is annotated as a Spring {@code @Service}, which enables the
 * autodetection and wiring of dependent implementation classes via
 * classpath scanning.
 */
@Service
public class AirportService {
    /**
     * An auto-wired field that connects the AirportService to the
     * AirportRepository that stores airline information persistently.
     */
    @Autowired
    private AirportRepository repository;

    /**
     * Returns a {@link List} of all known airports.
     *
     * @return A {@link List} of {@link Airport} objects
     */
    public List<Airport> getAirports() {
        return repository
            // Forward to the persistent repository.
            .findAll();
    }
}
