package server.airport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import server.common.model.Airport;

import static server.common.Constants.EndPoint.AIRPORTS;

/**
 * This Spring controller demonstrates how Spring MVC can be used to
 * handle an HTTP GET request via Java functional programming.  This
 * request is mapped to a method that synchronously returns a {@link
 * List} of all known {@code Airport} objects.
 *
 * In Spring's approach to building RESTful web services, HTTP
 * requests are handled by a controller (identified by the
 * {@code @RestController} annotation) that defines the endpoints (aka
 * routes) for each supported operation, i.e., {@code @GetMapping},
 * {@code @PostMapping}, {@code @PutMapping}, and
 * {@code @DeleteMapping}, which correspond to the HTTP GET, POST,
 * PUT, and DELETE calls, respectively.
 *
 * Spring uses the {@code @GetMapping} annotation to map HTTP GET
 * requests onto methods in the {@code AirportController}.  GET
 * requests invoked from any HTTP web client (e.g., a web browser or
 * Android app) or command-line utility (e.g., Curl or Postman).
 *
 * The {@code @ResponseBody} annotation tells a controller that the
 * object returned is automatically serialized into JSON and passed
 * back into the HttpResponse object.  The {@code @CrossOrigin}
 * annotation marks the annotated method or type as permitting cross
 * origin requests, which is required for Eureka redirection.
 */
@RestController
@CrossOrigin("*") // Required for Eureka redirection
public class AirportController {
    /**
     * This auto-wired field connects the {@link AirportController} to
     * the {@link AirportService}.
     */
    @Autowired
    AirportService service;

    /**
     * Request used by Eureka Control panel.
     *
     * @return Print something useful (e.g., the class name)
     */
    @GetMapping("/actuator/info")
    String info() {
        return getClass().getName();
    }

    /**
     * Return a {@link List} of all known airports.
     *
     * @return A {@link List} of {@link Airport} objects
     */
    @GetMapping(AIRPORTS)
    public List<Airport> getAirports() {
        return service
            // Forward to the AirportService.
            .getAirports();
    }
}
