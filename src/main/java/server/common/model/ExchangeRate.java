package server.common.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;

/**
 * This "Plain Old Java Object" (POJO) class keeps track of currency
 * to convert from and the currency to convert to.
 *
 * The {@code @Value} annotation assigns default values to
 * variables. 
 * 
 * The {@code @RequiredArgsConstructor} generates a constructor with 1
 * parameter for each field that requires special handling.  
 * 
 * The {@code @NoArgsConstructor} will generate a constructor with no
 * parameter.
 *
 * The {@code @Builder} annotation automatically creates a static
 * builder factory method for this class that can be used as follows:
 *
 * CurrencyConversion conversion = CurrencyConversion
 *   .builder()
 *   .to("CAN")
 *   .from("US")
 *   .build();
 *
 * The {@code @With} annotation generates a method that constructs a
 * clone of the object, but with a new value for this one field. 
 *
 * The {@code @Entity} annotation specifies that this class is an
 * entity and is mapped to a database table. 
 * 
 * The {@code @Table} annotation defines the name of the database
 * table that stores the exchange rate values.
 */
@Value
@RequiredArgsConstructor
@NoArgsConstructor(force = true)
@Builder
@With
@Entity // For JPA
@Table(name = "EXCHANGE_RATE") // For saving schema
public class ExchangeRate {
    /**
     * The {@code @Id} annotation indicates the {@code id} field below
     * is the primary key of an {@link ExchangeRate} object.  The
     * {@code @GeneratedValue} annotation configures the specified
     * field to auto-increment.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    /**
     * The name of the currency to convert from.
     */
    String fromCurrency;

    /**
     * The name of the currency to convert to.
     */
    String toCurrency;

    /**
     * The exchange rate for the "from" currency to the "to" currency.
     */
    double exchangeRate;
}
