package server.common;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * This class contains a {@code Bean} annotation that can be injected
 * into classes using the {@code @Autowired} annotation.  The {@code
 * LoadBalanced} annotation indicates the {@link RestTemplate} should
 * be based on client-side load balancing and checks Eureka server to
 * resolve the service name to host/port.
 */
@Component
public class Components {
    /**
     * This factory method returns a new {@link RestTemplate}, which
     * enables a synchronous client to perform HTTP requests.
     * 
     * @return A new {@link RestTemplate}
     */
    @Bean
    @LoadBalanced
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }
}
