package server.common;

/**
 * Static class used to centralize all constants used by the Flight
 * Listing App (FLAapp) server and all of its microservices.
 *
 * All HTTP requests URLs use the following convention:
 *
 * http:port//service/endpoint/strategy
 *
 * where "service" is one of the named microservices in the {@link
 * Service} inner class, "endpoint" is one of the endpoints declared
 * in the {@link EndPoint} inner class.
 */
public class Constants {
    /**
     * Each microservice will automatically register itself with the
     * Eureka service using its unique {@link Service} name string and
     * a randomly generated port. HTTP requests can then use the
     * microservice name instead of an IP address and port number.
     *
     * When the a microservice starts up Spring allocates an unused
     * random port (signaled by the {@code server.port=0} entry in the
     * {@code application.properties} file). This port is then used
     * when registering the microservice with the Eureka discovery
     * service (invoked by the {@code @EnableDiscoveryClient}
     * application annotation). Once registered, all requests to
     * microservices can simply use their registered application
     * names.
     *
     * For example, if the aa-airline microservice is allocated port
     * 40928 then a flights request URL will be mapped by the Eureka
     * server as follows:
     *
     * http://aa-airline/flights -> 192.168.7.23:40928/flights
     */
    public static class Service {
        /**
         * All service names must match the spring.application.name
         * property in each microservice application properties
         * resource file.
         *
         * All Airline services must include "airline" in their names
         * so that the {@code FlightService} class can determine which
         * microservices support the flight and best-price requests.
         */
        public static final String AIRPORT = "airport";
        public static final String EXCHANGE = "exchange";
        public static final String AA_AIRLINE = "aa-airline";
        public static final String SWA_AIRLINE = "swa-airline";
    }

    /**
     * All supported HTTP request endpoints.
     */
    public static class EndPoint {
        public static final String RATE = "rate";
        public static final String AIRPORTS = "airports";
        public static final String BEST_PRICE = "best-price";
        public static final String FLIGHTS = "flights";
        public static final String FLIGHT_DATES = "dates";
    }

    /**
     * Common resource file names used by all FLApp microservices,
     * which reside in the {@code src/main/resources} folder.
     */
    public static class Resources {
        public static final String EUREKA_CLIENT_PROPERTIES =
                "classpath:/common/eureka-client.properties";

        public static final String DATABASE_PROPERTIES =
                "classpath:/common/database.properties";
    }
}
