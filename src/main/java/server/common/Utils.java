package server.common;

import org.springframework.web.client.RestTemplate;

/**
 * A Java utility class that provides helper methods for dealing with
 * Spring web programming.
 */
public final class Utils {
    /**
     * A Java utility class should have a private constructor.
     */
    private Utils() {}

    /**
     * Make an HTTP GET call to the server passing in the {@code url}
     * and returning a result of type {@code T}.
     *
     * @param url The URL to pass to the server via a GET request
     * @param clazz The type {@code T} to return from GET
     * @return The result of type {@code T} from the server
     */
    public static <T> T makeGetRequest(RestTemplate restTemplate,
                                       String url,
                                       Class<T> clazz) {
        return restTemplate
            // Retrieve a representation by doing a GET on the URL.
            .getForEntity(url, clazz)

            // Returns the body of this entity.
            .getBody();
    }
}
