package server.airline.aa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import server.airline.common.AirlineRepository;
import server.common.Components;
import server.common.Constants;
import server.common.model.Flight;
import server.common.model.FlightRequest;

/**
 * This class provides the entry point for the American Airlines (AA)
 * microservice, which defines methods that find all available
 * flights, find the best price for a flight request, and find
 * departure dates for a given pair of airports.
 *
 * The {@code @SpringBootApplication} annotation enables apps to use
 * auto-configuration, component scan, and to define extra
 * configurations on their "application" class.  
 * 
 * The {@code @EnableDiscoveryClient} annotation enables service
 * registration and discovery, i.e., this process registers itself
 * with the discovery-server service using its application name.
 *
 * The {@code @EntityScan} annotation is used when entity classes are
 * not placed in the main application package or its sub-packages.
 *
 * The {@code @ComponentScan} annotation tells Spring the packages to
 * scan for annotated components (i.e., tagged with @Component).  
 * 
 * The {@code @PropertySources} annotation is used to provide
 * properties files to Spring Environment.  
 *
 * The {@code @EnableJpaRepositories} annotation enables the use of
 * JPA repositories by scanning the package of the annotated
 * configuration class for Spring Data repositories.
 */
@SpringBootApplication
@EnableDiscoveryClient
@EntityScan(basePackageClasses = {FlightRequest.class, Flight.class})
@ComponentScan(basePackageClasses = {
        AirlineRepository.class,
        AAApplication.class,
        Components.class})
@PropertySources({
        @PropertySource(Constants.Resources.EUREKA_CLIENT_PROPERTIES),
        @PropertySource(Constants.Resources.DATABASE_PROPERTIES),
        @PropertySource("classpath:/airline/aa/aa.properties")
})
@EnableJpaRepositories(basePackageClasses = AirlineRepository.class)
public class AAApplication {
    /**
     * A static main() entry point is needed to run the AA-airline
     * microservice.
     */
    public static void main(String[] args) {
        // Launch this microservice through Spring Boot.
        SpringApplication.run(AAApplication.class, args);
    }
}
