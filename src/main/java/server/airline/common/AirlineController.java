package server.airline.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

import server.common.model.Flight;
import server.common.model.FlightRequest;

import static server.common.Constants.EndPoint.BEST_PRICE;
import static server.common.Constants.EndPoint.FLIGHTS;
import static server.common.Constants.EndPoint.FLIGHT_DATES;

/**
 * This Spring controller demonstrates how Spring MVC can be used to
 * handle HTTP GET requests via Java functional programming.  These
 * requests are mapped to methods that synchronously find all
 * available flights, find the best price for a flight request, and
 * find departure dates for a given pair of airports.
 *
 * In Spring's approach to building RESTful web services, HTTP
 * requests are handled by a controller (identified by the
 * {@code @RestController} annotation) that defines the endpoints (aka
 * routes) for each supported operation, i.e., {@code @GetMapping},
 * {@code @PostMapping}, {@code @PutMapping}, and
 * {@code @DeleteMapping}, which correspond to the HTTP GET, POST,
 * PUT, and DELETE calls, respectively.
 *
 * Spring uses the {@code @GetMapping} annotation to map HTTP GET
 * requests onto methods in the {@linke AirlineController}.  GET
 * requests invoked from any HTTP web client (e.g., a web browser or
 * Android app) or command-line utility (e.g., Curl or Postman).
 *
 * The {@code @CrossOrigin} annotation marks the annotated method or
 * type as permitting cross origin requests, which is required for
 * Eureka redirection.
 * 
 * The {@code @ResponseBody} annotation tells a controller that the
 * object returned is automatically serialized into JSON and passed
 * back into the HttpResponse object.
 */
@RestController
@CrossOrigin("*") 
@ResponseBody
public class AirlineController {
    /**
     * This auto-wired field connects the {@link AirlineController} to
     * the {@link AirlineService}.
     */
    @Autowired
    AirlineService service;

    /**
     * Request used by Eureka Control panel.
     *
     * @return Print something useful (e.g., the class name)
     */
    @GetMapping("/actuator/info")
    String info() {
        return getClass().getName();
    }

    /**
     * Find all flight(s) that match the given {@code
     * departureAirport} and {@code arrivalAirport} on the given
     * {@code departureDate}.
     *
     * @param departureAirport The departure airport
     * @param departureDate The departure date
     * @param arrivalAirport The arrival airport
     * @return A {@link List} of matching {@link Flight} objects in
     *         the desired currency
     */
    @GetMapping(FLIGHTS)
    public List<Flight> findFlights(@RequestParam String departureAirport,
                                    @RequestParam String departureDate,
                                    @RequestParam String arrivalAirport) {
        return service
            // Forward to the AirlineService.
            .findFlights(departureAirport,
                         LocalDate.parse(departureDate),
                         arrivalAirport);
    }

    /**
     * Find the best priced flight(s) that match the given airports
     * and departure date.
     *
     * @param departureAirport The departure airport
     * @param departureDate The departure date
     * @param arrivalAirport   The arrival airport
     * @return A {@link List} containing {@link Flight} objects
     *         matching the {@link FlightRequest} that share the
     *         lowest price in the desired currency
     */
    @GetMapping(BEST_PRICE)
    List<Flight> findBestPrice(@RequestParam String departureAirport,
                               @RequestParam String departureDate,
                               @RequestParam String arrivalAirport) {
        return service
            // Forward to the AirlineService.
            .findBestPrice(departureAirport,
                           LocalDate.parse(departureDate),
                           arrivalAirport);
    }

    /**
     * Finds all departure dates that have at least one flight running
     * from the {@code departureAirport} to the {@code
     * arrivalAirport}.
     *
     * @param departureAirport The departure airport
     * @param arrivalAirport   The arrival airport
     * @return A {@link List} of all dates that match flights leaving
     *         the {@code departureAirport} and going to the {@code
     *         arrivalAirport}
     */
    @GetMapping(FLIGHT_DATES)
    public List<LocalDate> findDepartureDates(String departureAirport,
                                              String arrivalAirport) {
        return service
            // Forward to the AirlineService.
            .findDepartureDates(departureAirport,
                                arrivalAirport);
    }
}
