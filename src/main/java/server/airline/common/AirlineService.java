package server.airline.common;

import org.checkerframework.checker.units.qual.min;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import server.common.model.Flight;
import server.common.model.FlightRequest;

import static java.util.stream.Collectors.toList;

/**
 * This class defines implementation methods that are called by the
 * {@link AirlineController}.  These methods find all available
 * flights, find the best price for a flight request, and find
 * departure dates for a given pair of airports.
 *
 * This class is annotated as a Spring {@code @Service}, which enables
 * the autodetection and wiring of dependent implementation classes
 * via classpath scanning.
 */
@Service
public class AirlineService {
    /**
     * An auto-wired field that connects the {@link AirlineService} to
     * the {@link AirlineRepository} that stores airline information
     * persistently.
     */
    @Autowired
    AirlineRepository repository;

    /**
     * Find all flights that match the passed airports and departure date.
     *
     * @param departureAirport The departure airport
     * @param departureDate The departure date
     * @param arrivalAirport The arrival airport
     * @return A {@link List} containing all matching {@link Flight}
     * objects.
     */
    public List<Flight> findFlights(String departureAirport,
                                    LocalDate departureDate,
                                    String arrivalAirport) {
        return repository
            // Forward to the persistent repository.
            .findByDepartureAirportAndDepartureDateAndArrivalAirport(departureAirport,
                                                                     departureDate,
                                                                     arrivalAirport);
    }

    /**
     * Finds the best priced flights with the that match the passed
     * airports and departure date.
     *
     * @param departureAirport The departure airport
     * @param departureDate The departure date
     * @param arrivalAirport The arrival airport
     * @return A {@link List} containing {@link Flight} objects
     *         matching the {@link FlightRequest} that share the
     *         lowest price in the desired currency
     */
    public List<Flight> findBestPrice(String departureAirport,
                                      LocalDate departureDate,
                                      String arrivalAirport) {
        // Find the minimum price for the all matching flights.
        List<Flight> flights = findFlights(departureAirport,
                                           departureDate,
                                           arrivalAirport);

        // Return a List containing Flight objects matching the
        // FlightRequest that share the lowest price in the desired
        // currency.
        return flights
            // Convert the List of flights into a Stream. 
            .stream()

            // Find the minimum priced flight.
            .min(Comparator.comparingDouble(Flight::getPrice))

            // Find all flights that share the same minimum price
            // (assuming there are any flights).
            .flatMap(min -> Optional
                     .of(flights
                         // Convert the List into a Stream.
                         .stream()

                         // Allow all entries matching the min price.
                         .filter(flight -> flight
                                 .getPrice() == min.getPrice())))

            // Resolve the Optional to a Flight stream or an empty
            // stream.
            .orElseGet(Stream::empty)

            // Collect and return the results as a List.
            .collect(toList());
    }

    /**
     * Finds all departure dates that have at least one flight running
     * from the {@code departureAirport} to the {@code
     * arrivalAirport}.
     *
     * @param departureAirport The departure airport
     * @param arrivalAirport   The arrival airport
     * @return A {@link List} of all dates that match flights leaving
     *         the {@code departureAirport} and going to the {@code
     *         arrivalAirport}
     */
    public List<LocalDate> findDepartureDates(String departureAirport,
                                              String arrivalAirport) {
        return repository
            // Forward to the persistent repository.
            .findDepartureDates(departureAirport, arrivalAirport);
    }
}
