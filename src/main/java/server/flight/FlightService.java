package server.flight;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import server.common.Utils;
import server.common.model.Airport;
import server.common.model.ExchangeRate;
import server.common.model.Flight;
import server.common.model.FlightRequest;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static server.common.Constants.EndPoint.*;
import static server.common.Constants.Service.AIRPORT;
import static server.common.Constants.Service.EXCHANGE;

/**
 * This class defines implementation methods that are called by the
 * {@link FlightController}, which serves as the main "front-end" app
 * gateway entry point for remote clients.  These methods use Java
 * sequential and parallel streams to make synchronous Spring WebMVC
 * calls that find all available flights, find the best price for a
 * flight request, get a list of airports, and find departure dates
 * for a given pair of airports.
 *
 * This class is annotated as a Spring {@code @Service}, which enables
 * the autodetection and wiring of dependent implementation classes
 * via classpath scanning.
 *
 * A {@link DiscoveryClient} is used to redirect calls to the
 * appropriate microservices, which can run in processes that are
 * deployed to other computers in a cluster.
 */
@Service
public class FlightService {
    /**
     * This auto-wired field connects the {@link FlightService} to a
     * {@link RestTemplate} used to redirect all HTTP requests to the
     * appropriate microservices.
     */
    @Autowired
    RestTemplate restTemplate;

    /**
     * This auto-wired field connects the {@link FlightService} to the
     * {@link DiscoveryClient} used to find all registered
     * microservices.
     */
    @Autowired
    DiscoveryClient discoveryClient;

    /**
     * Tests can set this value for mocking a back-end server.
     */
    String baseUrl = "http://";

    /**
     * Find all flight(s) that match the given {@code
     * departureAirport} and {@code arrivalAirport} on the given
     * {@code departureDate} and update their price(s) to use the
     * desired {@code toCurrency}.
     *
     * @param departureAirport The departure airport
     * @param departureDate The departure date
     * @param arrivalAirport The arrival airport
     * @param toCurrency The desired currency
     * @return A {@link List} of matching {@link Flight} objects in
     *         the desired currency
     */
    public List<Flight> findFlights(String departureAirport,
                                    LocalDate departureDate,
                                    String arrivalAirport,
                                    String toCurrency) {
        // Find all flight(s) that match the given departureAirport
        // and arrivalAirport on the given departureDate and update
        // their price(s) to use the desired toCurrency.
        return findFlightsImpl(departureAirport,
                               departureDate,
                               arrivalAirport,
                               toCurrency,
                               FLIGHTS);
    }

    /**
     * Find the best priced flight(s) that match the given {@code
     * departureAirport} and {@code arrivalAirport} on the given
     * {@code departureDate} and update their price to use the desired
     * {@code toCurrency}.
     *
     * @param departureAirport The departure airport
     * @param departureDate The departure date
     * @param arrivalAirport The arrival airport
     * @param toCurrency The desired currency
     * @return A {@link List} containing {@link Flight} objects
     *         matching the {@link FlightRequest} that share the
     *         lowest price provided in the desired currency
     */
    private List<Flight> findBestPriceFlights(String departureAirport,
                                              LocalDate departureDate,
                                              String arrivalAirport,
                                              String toCurrency) {
        // Create a Map whose key is the currency and whose value is
        // the List of flights whose price is given in that currency.
        return findFlightsImpl(departureAirport,
                               departureDate,
                               arrivalAirport,
                               toCurrency,
                               BEST_PRICE);
    }

    /**
     * Use the operation designated by the HTTP {@code endpoint} to
     * find all flight(s) that match the given {@code
     * departureAirport} and {@code arrivalAirport} on the given
     * {@code departureDate} and update their price(s) to use the
     * desired {@code toCurrency}.
     *
     * @param departureAirport The departure airport
     * @param departureDate The departure date
     * @param arrivalAirport The arrival airport
     * @param toCurrency The desired currency
     * @param endpoint The HTTP endpoint operation to perform
     * @return A {@link List} of matching {@link Flight} objects in
     *         the desired currency
     */
    public List<Flight> findFlightsImpl(String departureAirport,
                                        LocalDate departureDate,
                                        String arrivalAirport,
                                        String toCurrency,
                                        String endpoint) {
        // Create a Map whose key is the currency and whose value is
        // the List of flights whose price is given in that currency.
        Map<String, List<Flight>> flightsMap = this
            // Use Eureka to get a list of Airline microservices.
            .getAirlineServices()

            // Process each microservice request in parallel.
            .parallelStream()

            // Use the HTTP endpoint to build a GET request URL to
            // forward to the designated Airline microservice.
            .map(airline -> 
                 makeAirlineUrl(airline,
                                endpoint,
                                departureAirport,
                                departureDate,
                                arrivalAirport))

            // Make an HTTP GET request on the designated Airline
            // microservice via Eureka redirection.
            .map(redirectGetUrl -> Utils
                 .makeGetRequest(restTemplate, 
                                 redirectGetUrl,
                                 Flight[].class))

            // Filter out any null array entries.
            .filter(Objects::nonNull)

            // Convert each array to a stream.
            .map(Arrays::stream)

            // Convert the Flight objects stream of streams into a stream
            // of Flight objects without using inefficient flatMap().
            .reduce(Stream::concat).orElse(Stream.empty())

            // Collect into a Map whose key is the currency and whose
            // value is the List of flights with that currency.
            .collect(Collectors.groupingBy(Flight::getCurrency,
                                           toList()));

        // Return a List of matching Flight objects in the desired
        // currency.
        return tryToConvertCurrency(toCurrency, flightsMap);
    }

    /**
     * Find the best priced flight(s) that match the given airports
     * and departure date.
     *
     * @param departureAirport The departure airport
     * @param departureDate The departure date
     * @param arrivalAirport The arrival airport
     * @param currency The desired currency
     * @return A {@link List} containing {@link Flight} objects
     *         matching the {@link FlightRequest} that share the
     *         lowest price provided in the desired currency
     */
    public List<Flight> findBestPrice(String departureAirport,
                                      LocalDate departureDate,
                                      String arrivalAirport,
                                      String currency) {
        // Find all the best priced flights matching the specified
        // parameters.
        List<Flight> flights = findBestPriceFlights(departureAirport,
                                                    departureDate,
                                                    arrivalAirport,
                                                    currency);

        // Return a List containing Flight objects matching the
        // FlightRequest that share the lowest price in the desired
        // currency.
        return flights
            // Convert the List of flights into a Stream. 
            .stream()

            // Find the minimum priced flight.
            .min(Comparator.comparingDouble(Flight::getPrice))

            // Find all flights that share the same minimum price
            // (assuming there are any flights).
            .flatMap(min -> Optional
                     .of(flights
                         // Convert the List into a Stream.
                         .stream()

                         // Allow all entries matching the min price.
                         .filter(flight -> flight
                                 .getPrice() == min.getPrice())))

            // Resolve the Optional to a Flight stream or an empty
            // stream.
            .orElseGet(Stream::empty)

            // Collect Stream into a List.
            .collect(toList());
    }

    /**
     * Finds all departure dates that have at least one flight running
     * from the {@code departureAirport} to the {@code
     * arrivalAirport}.
     *
     * @param departureAirport The departure airport
     * @param arrivalAirport   The arrival airport
     * @return A {@link List} of all dates that match flights leaving
     *         the {@code departureAirport} and going to the {@code
     *         arrivalAirport}
     */
    public List<LocalDate> findDepartureDates(String departureAirport,
                                              String arrivalAirport) {
        // Return a List of all matching dates.
        return this
            // Use Eureka to get a list of Airline microservices.
            .getAirlineServices()

            // Process each Airline microservice request in parallel.
            .parallelStream()

            // Build a FLIGHT_DATES GET request URL to forward to the
            // designated Airline microservice.
            .map(airline -> 
                 makeAirlineUrl(airline,
                                FLIGHT_DATES,
                                departureAirport,
                                null,
                                arrivalAirport))

            // Make an HTTP GET request on the designated Airline
            // microservice via Eureka redirection.
            .map(redirectGetUrl -> Utils
                 .makeGetRequest(restTemplate,
                                 redirectGetUrl,
                                 LocalDate[].class))

            // Filter out any null array entries.
            .filter(Objects::nonNull)

            // Convert array to a stream.
            .map(Arrays::stream)

            // Convert the stream of streams of Flights into a stream
            // of Flights without using flatMap().
            .reduce(Stream::concat).orElse(Stream.empty())

            // Remove duplicates.
            .distinct()

            // Collect the LocalDate objects into a List.
            .collect(toList());
    }

    /**
     * Redirects request to airport microservice to return all known
     * airports.
     *
     * @return A {@link List} that contains {@link Airport} objects
     */
    public List<Airport> getAirports() {
        // Make an HTTP GET request on the designated Airport
        // microservice via Eureka redirection.
        Airport[] airports = Utils
            .makeGetRequest(restTemplate,
                            makeAirportsUrl(),
                            Airport[].class);

        // Return a List of any/all Airport objects.
        return airports != null 
            // Convert the array into a List.
            ? Arrays.asList(airports) 

            // Return an empty List.
            : Collections.emptyList();
    }

    /**
     * Use the Eureka discovery client to find all airline
     * microservices.
     *
     * @return A {@link List} of all registered Airline
     *         microservices
     */
    private List<String> getAirlineServices() {
        // Return a List of all registered Airline microservices.
        return discoveryClient
            // Get a List of all registered microservices.
            .getServices()

            // Convert the List into a Stream.
            .stream()

            // Only include the airline microservices.
            .filter(id -> id.toLowerCase().contains("airline"))

            // Collect the results into a List.
            .collect(toList());
    }

    /**
     * Attempt to convert the price of all the flights in the {@code
     * flightsMap} to the desired {@code toCurrency}, but do nothing
     * for any flights that are already in the desired currency.
     *
     * @param toCurrency The currency to convert to
     * @param flightsMap A {@link Map} whose key is the currency and
     *                   whose value is the {@link List} of {@link
     *                   Flight} objects whose price is given in that
     *                   currency
     * @return A {@link List} of {@link Flight} objects whose price is
     *         now in {@code toCurrency} format
     */
    private List<Flight> tryToConvertCurrency(String toCurrency,
                                              Map<String, List<Flight>> flightsMap) {
        return flightsMap
            // Obtain a Set of key/value pairs from the Map.
            .entrySet()

            // Convert the Set to a Stream.
            .stream()

            // Try to convert the price of all flights, if necessary.
            .flatMap(entry ->
                     tryToConvertCurrency(entry.getKey(),
                                          toCurrency,
                                          entry.getValue()))

            // Collect the results into a List.
            .collect(toList());
    }

    /**
     * Attempt to convert the price of all the {@code flights} from
     * the {@code fromCurrency} to the {@code toCurrency}, but do
     * nothing if these two currencies are the same.
     *
     * @param fromCurrency The currency to convert from
     * @param toCurrency The currency to convert to
     * @param flights The {@link List} of flights to convert the
     *                currency
     * @return A {@link Stream} of {@link Flight} objects whose price
     *         is now in {@code toCurrency} format
     */
    private Stream<Flight> tryToConvertCurrency(String fromCurrency,
                                                String toCurrency,
                                                List<Flight> flights) {
        // Only perform the conversion if it's necessary.
        if (!fromCurrency.equals(toCurrency)) {
            // Make an HTTP GET request on the ExchangeRate
            // microservice via Eureka redirection.
            ExchangeRate rate = Utils
                .makeGetRequest(restTemplate,
                                makeExchangeUrl(fromCurrency,
                                                toCurrency),
                                ExchangeRate.class);

            if (rate == null)
                throw new IllegalArgumentException("no conversion possible");

            // Return a stream of flights whose price has been
            // converted into the toCurrency.
            return flights
                // Convert the List of flights into a stream.
                .stream()

                // Update the price based on the exchange rate.
                .map(flight -> flight
                     .withPrice(flight.getPrice() * rate.getExchangeRate()));
        } else {
            // Return a stream of flights whose price has not been
            // converted since it was already in the desired currency.
            return flights
                // Convert the list of flights into a Stream.
                .stream();
        }
    }

    /**
     * Build a GET request URL to forward to the designated Airline
     * microservice.
     *
     * @param airlineMicroservice The microservice to forward the
     *                            request to
     * @param endpointMethod The endpoint method being called
     * @param departureAirport The departure airport
     * @param departureDate The departure date
     * @param arrivalAirport The arrival airport
     * @return A {@link String} containing the URL to send via an HTTP 
     *         GET request to the {@code airlineMicroservice}
     */
    private String makeAirlineUrl(String airlineMicroservice,
                                  String endpointMethod,
                                  String departureAirport,
                                  LocalDate departureDate,
                                  String arrivalAirport) {
        // Build a GET request URL along with query parameters to
        // forward to the designated Airline microservice.
        return baseUrl
            + airlineMicroservice
            + "/" 
            + endpointMethod
            + "?departureAirport=" 
            + departureAirport  
            + (departureDate != null
               ? ("&departureDate=" + departureDate)
               : (""))
            + "&arrivalAirport=" 
            + arrivalAirport;
    }

    /**
     * Make an HTTP GET request on the {@link ExchangeRate}
     * microservice via Eureka redirection.
     * 
     * @param fromCurrency The currency to convert from
     * @param toCurrency The currency to convert to
     * @return A {@link String} containing the URL to send via an HTTP
     *         GET request to the {@link ExchangeRate} microservice
     */
    private String makeExchangeUrl(String fromCurrency,
                                   String toCurrency) {
        return baseUrl
            + EXCHANGE
            + "/" 
            + RATE
            + "?fromCurrency" 
            + fromCurrency
            + "&toCurrency" 
            + toCurrency;
    }

    /**
     * Build an AIRPORT request URL to forward to the {@link Airport}
     * microservice.
     *
     * @return A {@link String} containing the URL to send via an HTTP
     *         GET request to the {@link Airport} microservice
     */
    private String makeAirportsUrl() {
        return baseUrl 
            + AIRPORT 
            + "/" 
            + AIRPORTS;
    }
}
