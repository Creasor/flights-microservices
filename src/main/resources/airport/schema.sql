create table airport (
    airport_code varchar(3) not null,
    airport_name varchar(255),
    primary key (airport_code)
);
