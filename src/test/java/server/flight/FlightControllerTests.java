package server.flight;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import server.airline.FlightFactory;
import server.common.Util;
import server.common.model.Airport;
import server.common.model.Flight;
import server.common.model.FlightRequest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.clearInvocations;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static server.common.Constants.EndPoint.AIRPORTS;
import static server.common.Constants.EndPoint.BEST_PRICE;
import static server.common.Constants.EndPoint.FLIGHTS;
import static server.common.Constants.EndPoint.FLIGHT_DATES;

@SpringBootTest(classes = FlightApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@AutoConfigureMockMvc
public class FlightControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private FlightService service;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void testFindFlights() throws Exception {
        List<Flight> expected = FlightFactory.builder().build();
        FlightRequest flightRequest = FlightFactory.buildRequestFrom(expected.get(0));

        when(service.findFlights(
                flightRequest.getDepartureAirport(),
                flightRequest.getDepartureDate(),
                flightRequest.getArrivalAirport(),
                flightRequest.getCurrency()))
                .thenReturn(expected);

        String jsonResult = mockMvc.perform(
                MockMvcRequestBuilders
                        .get("/" + FLIGHTS)
                        .queryParam("departureAirport", flightRequest.getDepartureAirport())
                        .queryParam("departureDate", Util.dateString(flightRequest.getDepartureDate()))
                        .queryParam("arrivalAirport", flightRequest.getArrivalAirport())
                        .queryParam("currency", flightRequest.getCurrency())).andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        List<Flight> result = objectMapper.readValue(
                jsonResult, new TypeReference<List<Flight>>() {
                });

        List<Flight> cleanedResult = result.stream()
                .map(flight -> flight.withId(null))
                .collect(Collectors.toList());

        verify(service, times(1)).findFlights(
                flightRequest.getDepartureAirport(),
                flightRequest.getDepartureDate(),
                flightRequest.getArrivalAirport(),
                flightRequest.getCurrency());

        assertThat(cleanedResult).isEqualTo(expected);

        clearInvocations(service);
    }

    @Test
    public void testFindBestPrice() throws Exception {
        List<Flight> flights = FlightFactory.builder().build();

        for (Flight expected : flights) {
            FlightRequest flightRequest =
                    FlightFactory.buildRequestFrom(expected);

            List<Flight> expectedList = new ArrayList<>(1);
            expectedList.add(expected);

            when(service.findBestPrice(
                    flightRequest.getDepartureAirport(),
                    flightRequest.getDepartureDate(),
                    flightRequest.getArrivalAirport(),
                    flightRequest.getCurrency()))
                    .thenReturn(expectedList);

            String jsonResult = mockMvc.perform(
                    MockMvcRequestBuilders
                            .get("/" + BEST_PRICE)
                            .queryParam("departureAirport", flightRequest.getDepartureAirport())
                            .queryParam("departureDate", Util.dateString(flightRequest.getDepartureDate()))
                            .queryParam("arrivalAirport", flightRequest.getArrivalAirport())
                            .queryParam("currency", flightRequest.getCurrency())).andExpect(status().isOk())
                    .andReturn()
                    .getResponse()
                    .getContentAsString();

            List<Flight> result = objectMapper.readValue(
                    jsonResult, new TypeReference<List<Flight>>() {
                    });

            List<Flight> cleanedResult = result.stream()
                    .map(flight -> flight.withId(null))
                    .collect(Collectors.toList());

            verify(service, times(1)).findBestPrice(
                    flightRequest.getDepartureAirport(),
                    flightRequest.getDepartureDate(),
                    flightRequest.getArrivalAirport(),
                    flightRequest.getCurrency());

            assertThat(cleanedResult).isEqualTo(expectedList);

            clearInvocations(service);
        }
    }

    @Test
    public void testFindFlightDates() throws Exception {
        List<LocalDate> expected = new ArrayList<>(2);
        expected.add(LocalDate.parse("1234-01-01"));
        expected.add(LocalDate.parse("1234-01-02"));
        String from = "from here";
        String to = "to there";

        when(service.findDepartureDates(from, to)).thenReturn(expected);

        String jsonResult = mockMvc.perform(
                MockMvcRequestBuilders.get("/" + FLIGHT_DATES)
                        .queryParam("departureAirport", from)
                        .queryParam("arrivalAirport", to))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        List<LocalDate> result = objectMapper.readValue(
                jsonResult, new TypeReference<List<LocalDate>>() {
                });

        assertThat(result).isEqualTo(expected);

        verify(service, times(1)).findDepartureDates(from, to);
    }

    @Test
    public void testGetAirports() throws Exception {
        List<Airport> expected = List.of(
                new Airport("ABC", "ABC description"),
                new Airport("DEF", "DEF description"),
                new Airport("HIJ", "HIJ description"));

        when(service.getAirports()).thenReturn(expected);

        String jsonResult = mockMvc.perform(
                MockMvcRequestBuilders.get("/" + AIRPORTS))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        List<Airport> result = objectMapper.readValue(
                jsonResult, new TypeReference<List<Airport>>() {
                });

        verify(service, times(1)).getAirports();

        assertThat(result).isEqualTo(expected);
    }
}