package server.flight;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import server.airline.FlightFactory;
import server.common.model.Flight;
import server.common.model.FlightRequest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

/**
 * These use mocking to isolate and test only the service component.
 */
@AutoConfigureMockMvc
@DataJpaTest(properties = "spring.datasource.data=")
@ContextConfiguration(classes = {FlightApplication.class, FlightService.class})
public class FlightServiceTests {

    private static MockWebServer mockBackEnd;
    private static ObjectMapper objectMapper;
    private FlightService service;

    @MockBean
    DiscoveryClient discoveryClientMock;

    /**
     * Test using a few airline microservices.
     */
    private final List<String> airlineCodes = List.of("1", "2", "3");

    @BeforeAll
    static void setUp() throws IOException {
        mockBackEnd = new MockWebServer();
        mockBackEnd.start();
        objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    }

    @AfterAll
    static void tearDown() throws IOException {
        mockBackEnd.shutdown();
    }

    @BeforeEach
    void beforeEach() {
        service = new FlightService();
        service.baseUrl = String.format("http://localhost:%d/", mockBackEnd.getPort());
        service.restTemplate = new RestTemplate();

        // Create mock airline microservices that are returned
        // when the service call the discoveryClient.getServices()
        // to enumerate over all known airline microservices.
        List<String> airlineServices = airlineCodes
                .stream()
                .map(code -> code + "-airline")
                .collect(Collectors.toList());
        when(discoveryClientMock.getServices()).thenReturn(airlineServices);
        service.discoveryClient = discoveryClientMock;
    }

    @Test
    public void testFindFlights() throws Exception {
        List<Flight> expected = new ArrayList<>();

        for (String airlineCode : airlineCodes) {
            List<Flight> flights = buildExpectedFlights(airlineCode);
            mockBackEnd.enqueue(new MockResponse()
                    .setBody(objectMapper.writeValueAsString(flights))
                    .addHeader("Content-Type", "application/json"));
            expected.addAll(flights);
        }

        FlightRequest flightRequest = FlightFactory.buildRequestFrom(expected.get(0));

        assertThat(new HashSet<>(service.findFlights(
                flightRequest.getDepartureAirport(),
                flightRequest.getDepartureDate(),
                flightRequest.getArrivalAirport(),
                flightRequest.getCurrency())))
                .isEqualTo(new HashSet<>(expected));
    }

    @Test
    public void testFindBestPrice() throws Exception {
        // Build expected FLIGHT request result lists for
        // each airline and then add them to the mock
        // back-end queue.
        List<Flight> expected = new ArrayList<>();
        List<Flight> bestPrice = new ArrayList<>();

        for (String airlineCode : airlineCodes) {
            List<Flight> nextExpected = buildExpectedFlights(airlineCode);

            // Force a flight to have a lower price that all others in this list.
            Flight firstFlight = nextExpected.get(0);
            Flight reducedPrice = firstFlight.withPrice(1.0);
            nextExpected.set(nextExpected.indexOf(firstFlight), reducedPrice);
            bestPrice.add(reducedPrice);

            mockBackEnd.enqueue(new MockResponse()
                    .setBody(objectMapper.writeValueAsString(nextExpected))
                    .addHeader("Content-Type", "application/json"));

            expected.addAll(nextExpected);
        }

        // We know that the first flight of the combined list of expected
        // results will return the correct flights from each of the
        // airline services.
        FlightRequest flightRequest = FlightFactory.buildRequestFrom(expected.get(0));

        // Sanity check to ensure that the expected list of best priced flights
        // will all match a request parameter taken from the first random flight.
        assertThat(bestPrice.stream().allMatch(flight ->
                flight.getDepartureAirport()
                        .equals(flightRequest.getDepartureAirport()) &&
                flight.getDepartureDate()
                        .equals(flightRequest.getDepartureDate()) &&
                flight.getArrivalAirport()
                        .equals(flightRequest.getArrivalAirport()))
        ).isTrue();

        // Since the service uses parallel concurrency when forwarding
        // requests to microservices, there is no guarantee of the order
        // in which they will be serviced so convert to HashSets before
        // comparing.
        assertThat(new HashSet<>(service.findBestPrice(
                flightRequest.getDepartureAirport(),
                flightRequest.getDepartureDate(),
                flightRequest.getArrivalAirport(),
                flightRequest.getCurrency())))
                .isEqualTo(new HashSet<>(bestPrice));
    }

    @Test
    public void testFindFlightDates() throws Exception {
        List<LocalDate> expected = new ArrayList<>();
        List<Flight> allFlights = new ArrayList<>();

        for (String airlineCode : airlineCodes) {
            int days = 4;
            List<Flight> flights = FlightFactory.builder()
                    .airlines(airlineCode)
                    .airports(3)
                    .dailyFlights(1)
                    .from(LocalDate.now())
                    .to(LocalDate.now().plusDays(days - 1))
                    .build();
            allFlights.addAll(flights);

            List<LocalDate> dates = flights.stream().filter(flight ->
                    flight.getDepartureAirport()
                            .equals(allFlights.get(0).getDepartureAirport()) &&
                    flight.getArrivalAirport()
                            .equals(allFlights.get(0).getArrivalAirport()))
                    .map(Flight::getDepartureDate)
                    .distinct()
                    .collect(Collectors.toList());

            assertThat(dates).hasSize(days);

            mockBackEnd.enqueue(new MockResponse()
                    .setBody(objectMapper.writeValueAsString(dates))
                    .addHeader("Content-Type", "application/json"));

            expected.addAll(dates);
        }

        // There should be 12 distinct dates.
        assertThat(expected).hasSize(12);

        assertThat(new HashSet<>(service.findDepartureDates(
                allFlights.get(0).getDepartureAirport(),
                allFlights.get(0).getArrivalAirport()))
        ).isEqualTo(new HashSet<>(expected));
    }

    /**
     * Builds a list of flights matching a flight request built using the
     * first flight in a random list of flights. This method should only
     * be used for FLIGHT and BEST_PRICE requests.
     *
     * @param airlineCode All flights will use this airline code.
     * @return A list that can be enqueued using the Okhttp3 mock server.
     */
    private List<Flight> buildExpectedFlights(String airlineCode) {
        List<Flight> flights = FlightFactory.builder().airlines(airlineCode).build();
        FlightRequest flightRequest = FlightFactory.buildRequestFrom(flights.get(0));

        return flights.stream()
                .filter(flight ->
                        flight.getAirlineCode().equals(airlineCode) &&
                        flight.getDepartureAirport()
                                .equals(flightRequest.getDepartureAirport()) &&
                        flight.getDepartureDate()
                                .equals(flightRequest.getDepartureDate()) &&
                        flight.getArrivalAirport()
                                .equals(flightRequest.getArrivalAirport()))
                .collect(Collectors.toList());
    }
}