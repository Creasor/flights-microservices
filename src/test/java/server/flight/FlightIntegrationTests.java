package server.flight;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.List;

import server.airline.FlightFactory;
import server.common.Util;
import server.common.model.Flight;
import server.common.model.FlightRequest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static server.common.Constants.EndPoint.BEST_PRICE;
import static server.common.Constants.EndPoint.FLIGHTS;

@SpringBootTest(
        classes = {server.flight.FlightApplication.class},
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ContextConfiguration(classes = {FlightApplication.class})
public class FlightIntegrationTests {

    @MockBean
    public RestTemplate restTemplate;

    @MockBean
    public DiscoveryClient discoveryClient;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Mock
    ResponseEntity<Flight[]> responseMock1;

    @Mock
    ResponseEntity<Flight[]> responseMock2;

    /**
     * Brittle test because it mocks RestTemplate instead of
     * actually mocking the back-end server.
     */
    //@@Doug - can't run this test until Doug is finished.
    // @Test
    public void testFindFlights() throws Exception {
        List<Flight> flights = FlightFactory.builder().build();
        Flight flight1 = flights.get(0);
        Flight flight2 = flights.get(1);

        assertNotEquals(flight1, flight2);
        List<Flight> expected = List.of(flight1, flight2);

        FlightRequest request = FlightFactory.buildRequestFrom(flight1);

        Flight[] response1 = new Flight[]{flight1};
        Flight[] response2 = new Flight[]{flight2};
        List<String> airlines = List.of("airline-1", "airline-2");

        when(restTemplate
                .postForEntity(
                        "http://" + airlines.get(0) + "/" + FLIGHTS,
                        request, Flight[].class))
                .thenReturn(responseMock1);
        when(responseMock1.getBody()).thenReturn(response1);
        when(restTemplate
                .getForEntity(
                        "http://" + airlines.get(1) + "/" + FLIGHTS,
                        Flight[].class))
                .thenReturn(responseMock2);
        when(responseMock2.getBody()).thenReturn(response2);
        when(discoveryClient.getServices()).thenReturn(airlines);

        String jsonResult = mockMvc.perform(
                MockMvcRequestBuilders.get("/" + BEST_PRICE)
                        .queryParam("departureAirport", request.getDepartureAirport())
                        .queryParam("departureDate", Util.dateString(request.getDepartureDate()))
                        .queryParam("arrivalAirport", request.getArrivalAirport())
                        .queryParam("currency", request.getCurrency()))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        verify(restTemplate, times(1)).getForEntity(
                "http://" + airlines.get(0) + "/" + FLIGHTS,
                Flight[].class);

        verify(restTemplate, times(1)).postForEntity(
                "http://" + airlines.get(1) + "/" + FLIGHTS,
                request, Flight[].class);

        List<Flight> result = objectMapper.readValue(
                jsonResult, new TypeReference<List<Flight>>() {
                });

        assertEquals(expected, result);
    }
}