package server.airline;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import server.airline.aa.AAApplication;
import server.airline.common.AirlineRepository;
import server.airline.common.AirlineService;
import server.common.model.Flight;
import server.common.model.FlightRequest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * These use mocking to isolate and test only the service component.
 */

@SpringBootTest(properties = "spring.datasource.data=")
@AutoConfigureMockMvc
@ContextConfiguration(classes = {AAApplication.class, AirlineService.class})
public class AirlineServiceTests {

    @Autowired
    AirlineService service;

    @MockBean
    AirlineRepository repository;

    @Test
    public void testFindFlights() {
        List<Flight> expected = FlightFactory.builder().build();
        FlightRequest flightRequest =
                FlightFactory.buildRequestFrom(expected.get(0));

        when(repository
                .findByDepartureAirportAndDepartureDateAndArrivalAirport(
                        flightRequest.getDepartureAirport(),
                        flightRequest.getDepartureDate(),
                        flightRequest.getArrivalAirport()
                )
        ).thenReturn(expected);

        assertThat(service.findFlights(
                flightRequest.getDepartureAirport(),
                flightRequest.getDepartureDate(),
                flightRequest.getArrivalAirport())
        ).isEqualTo(expected);

        verify(repository, times(1))
                .findByDepartureAirportAndDepartureDateAndArrivalAirport(
                        flightRequest.getDepartureAirport(),
                        flightRequest.getDepartureDate(),
                        flightRequest.getArrivalAirport());
    }

    @Test
    public void testFindBestPrice() {
        List<Flight> flights = FlightFactory.builder().build();

        Flight anyFlight = flights.get(0);
        FlightRequest flightRequest = FlightFactory.buildRequestFrom(anyFlight);

        List<Flight> candidates =
                flights.stream()
                        .filter(flight ->
                                flight.getDepartureAirport()
                                        .equals(anyFlight.getDepartureAirport()) &&
                                flight.getArrivalAirport()
                                        .equals(anyFlight.getArrivalAirport()) &&
                                flight.getDepartureDate()
                                        .equals(anyFlight.getDepartureDate()))
                        .collect(Collectors.toList());

        double min = candidates.stream()
                .map(Flight::getPrice)
                .min(Double::compareTo)
                .orElseThrow();

        List<Flight> expected = candidates.stream()
                .filter(flight -> flight.getPrice() == min)
                .collect(Collectors.toList());

        assertThat(expected).isNotEmpty();

        when(repository
                .findByDepartureAirportAndDepartureDateAndArrivalAirport(
                        flightRequest.getDepartureAirport(),
                        flightRequest.getDepartureDate(),
                        flightRequest.getArrivalAirport()
                )
        ).thenReturn(candidates);

        List<Flight> response = service.findBestPrice(
                flightRequest.getDepartureAirport(),
                flightRequest.getDepartureDate(),
                flightRequest.getArrivalAirport());

        assertThat(response).isEqualTo(expected);

        verify(repository, times(1))
                .findByDepartureAirportAndDepartureDateAndArrivalAirport(
                        flightRequest.getDepartureAirport(),
                        flightRequest.getDepartureDate(),
                        flightRequest.getArrivalAirport());
    }

    @Test
    public void testFindFlightDates() {
        List<LocalDate> expected = new ArrayList<>(2);
        expected.add(LocalDate.parse("1234-01-01"));
        expected.add(LocalDate.parse("1234-01-02"));
        String destinationAirport = "from here";
        String arrivalAirport = "to there";

        when(repository.findDepartureDates(destinationAirport, arrivalAirport))
                .thenReturn(expected);

        assertThat(service.findDepartureDates(destinationAirport, arrivalAirport))
                .isEqualTo(expected);

        verify(repository, times(1))
                .findDepartureDates(destinationAirport, arrivalAirport);
    }
}