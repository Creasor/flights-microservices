package server.airport;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import server.common.model.Airport;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(properties = "spring.datasource.data=")
@AutoConfigureMockMvc
public class AirportControllerTests {

    @MockBean
    private AirportService service;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void testFindAllAirports() throws Exception {

        List<Airport> expected = new ArrayList<>(3);
        expected.add(new Airport("ABC", "ABC description"));
        expected.add(new Airport("DEF", "DEF description"));
        expected.add(new Airport("HIJ", "HIJ description"));

        when(service.getAirports()).thenReturn(expected);

        String jsonResult = mockMvc.perform(
                MockMvcRequestBuilders.get("/airports"))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        List<Airport> result = objectMapper.readValue(
                jsonResult, new TypeReference<List<Airport>>() {
                });

        assertThat(result).isEqualTo(expected);
    }
}
