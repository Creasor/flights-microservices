package server.exchange;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import server.common.model.ExchangeRate;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(properties = "spring.datasource.data=")
@AutoConfigureMockMvc
public class ExchangeControllerTests {

    @MockBean
    private ExchangeService service;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void testFindAllExchangeRates() throws Exception {

        List<ExchangeRate> exchangeRates = new ArrayList<>(3);
        exchangeRates.add(new ExchangeRate(null, "ABC", "DEF", 0.7));
        exchangeRates.add(new ExchangeRate(null, "DEF", "ABC", 0.3));
        exchangeRates.add(new ExchangeRate(null, "HIJ", "ABC", 2.2));

        for (ExchangeRate exchangeRate : exchangeRates) {
            when(service.getRate(
                    exchangeRate.getFromCurrency(),
                    exchangeRate.getToCurrency())
            ).thenReturn(exchangeRate);
        }

        for (ExchangeRate expected : exchangeRates) {
            String jsonResult = mockMvc.perform(
                    MockMvcRequestBuilders.get("/rate")
                            .queryParam("fromCurrency", expected.getFromCurrency())
                            .queryParam("toCurrency", expected.getToCurrency()))
                    .andExpect(status().isOk())
                    .andReturn()
                    .getResponse()
                    .getContentAsString();

            ExchangeRate result = objectMapper.readValue(jsonResult, ExchangeRate.class);

            assertThat(result).isEqualTo(expected);
        }

        for (ExchangeRate exchangeRate : exchangeRates) {
            verify(service, times(1)).getRate(
                    exchangeRate.getFromCurrency(),
                    exchangeRate.getToCurrency());
        }
    }

    @Test
    public void testInvalidExchangeRate() throws Exception {
        when(service.getRate(any(), any())).thenReturn(null);

        String jsonResult = mockMvc.perform(
                MockMvcRequestBuilders.get("/rate")
                        .queryParam("fromCurrency", "ABC")
                        .queryParam("toCurrency", "DEF"))
                .andExpect(status().isNoContent())
                .andReturn()
                .getResponse()
                .getContentAsString();

        assertThat(jsonResult).isNullOrEmpty();
    }
}
